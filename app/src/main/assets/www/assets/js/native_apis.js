// Native-APIS.js
// Builds in HTML-5 APIs for Native Functions

//HTML5 API for Video, Audio & Camera:
    function hasGetUserMedia() {
      return !!(navigator.getUserMedia || navigator.webkitGetUserMedia ||
                navigator.mozGetUserMedia || navigator.msGetUserMedia);
    }

    if (hasGetUserMedia()) {
      //alert('getUserMedia() is supported! Nice!');
    } else {
      //alert('getUserMedia() is not supported in your browser');
    }


//HTML5 API for Battery:
    window.addEventListener("load", function() {
if (window.navigator.battery!=undefined || window.navigator.mozBattery!=undefined) {
    var output = document.getElementById("output");
    var battery = window.navigator.battery || window.navigator.mozBattery;

    output.innerHTML += "<p>Level: " + battery.level;
    output.innerHTML += "<p>Discharging time: " + battery.dischargingTime;
    output.innerHTML += "<p>Charging?: " + battery.charging;
} else {
    alert("Battery Status API is not available");
}

});

//HTML5 API for Connection Type:
if (navigator.connection!=undefined) {
    alert ("Connection type: " + navigator.connection.type);
} else {
    alert("Connection API not available");
}

//HTML5 API for Notifications:
function notifyLater() {
    setTimeout(notify, 3000);
}

var notificationManager = navigator.mozNotification || navigator.webkitNotification;

function notify() {
    if (notificationManager!=undefined) {
        notification.show();
    } else {
        alert("Notifications not available");
    }
}

if (notificationManager!=undefined) {
    var notification = notificationManager.createNotification("HTML5 Notification!", "Hello from the website!");
}

notification.onclick = function() {
    alert('You clicked the notification');
};